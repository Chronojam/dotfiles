let
  # Keep it reproducable by tying pkg version
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/47db3772a731d48f30d6dc97120ef77f5240096c.tar.gz") {
    config = {
      allowUnfreePredicate = pkg: builtins.elem (pkgs.lib.getName pkg) [
        "1password-cli"
      ];
    };
  };
  tf-pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/a2af67230b8e05816b2aed301a56a70d5161b9f6.tar.gz") {};

  # We need kubectl 1.24.0, which needs go 1.18 to build.
  kubernetes_1_24 = (pkgs.kubernetes.override {
    buildGoModule = pkgs.buildGo118Module;
  }).overrideAttrs (old: rec {
    version = "v1.24.0";
    src = pkgs.fetchFromGitHub {
      owner = "kubernetes";
      repo  = "kubernetes";
      rev   = "v1.24.0";
      sha256 = "sha256-B5xA5StldfjK3R5PBWM/WI7j8p5RDmgJYuOwPf1J0Ro=";
    };
  });
  kubectl_1_24 = (pkgs.kubectl.override {
    buildGoModule = pkgs.buildGo118Module;
    kubernetes = kubernetes_1_24;
  });

  jsonnet-tool = (pkgs.buildGoModule rec {
    pname = "jsonnet-tool";
    version = "1.0.0";
    src = pkgs.fetchFromGitLab {
      owner = "gitlab-com/gl-infra";
      repo  = "jsonnet-tool";
      rev   = "e650ec91";
      sha256 = "sha256-hZWOdMYgxnV1WjCvWe9yxhEK/unPfe5ntrSLCubhdMQ="; 
    };
    vendorSha256 = "sha256-6j2OdROxj23SzvuVWk7iFFDVgfWqBgrz54fvLuSb5cs=";
  });
in 
{
  services.pcscd.enable = true;
  environment = {
  # Desktop specific packages.
    systemPackages = with pkgs; [
      # gubbins
      google-cloud-sdk
      chefdk
      teleport
      tf-pkgs.terraform # 1.2.2
      doctl
      aws
      kubernetes-helm
      minikube
      vagrant
      sshuttle
      jq
      kubectx
      kubectl_1_24
      jsonnet-tool
      _1password
      vault-bin
      vscodium-fhs
    
      # yubikey
      yubikey-personalization
      libu2f-host
      pcsctools
    ];
  };

  # Enable vagrant, with NFS
  services.nfs.server.enable = true;
  networking.firewall.extraCommands = ''
    iptables -I INPUT 1 -i vboxnet+ -p tcp -m tcp --dport 2049 -j ACCEPT
  '';

  # Virtualbox
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "calliope" ];
}
