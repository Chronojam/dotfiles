{ config, pkgs, ... }:
let 
  extensions = (with pkgs.vscode-extensions; [
    ms-dotnettools.csharp
   #ms-vscode.mono-debug
   # neikeq.godot-csharp-vscode
  ]) ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
    {
      name = "nix-extension-pack";
      publisher = "pinage404";
      version = "1.0.0";
      sha256 = "10hi9ydx50zd9jhscfjiwlz3k0v4dfi0j8p58x8421rk5dspi98x";
    }
    {
      name = "mono-debug";
      publisher = "ms-vscode";
      version = "0.16.2";
      sha256 = "o6QPC+Yzy7O3icp3D2289kMC+bLhd+QT1sgbyAhqTRQ=";
    }
    {
      name = "godot-csharp-vscode";
      publisher = "neikeq";
      version = "0.2.1";
      sha256 = "sLsP+4deo/O8NjHGGXVdSOPWQPALypW/H0oZOMMM9RE=";
    }
  ];
  vscodium-with-extensions =  pkgs.vscode-with-extensions.override {
    vscode = pkgs.vscodium;
    vscodeExtensions = extensions;
  };
in {

    # Use the GRUB 2 boot loader.
    boot.loader.grub.enable = true;
    boot.loader.grub.copyKernels = true;
    boot.loader.grub.efiSupport = true;
    boot.loader.grub.efiInstallAsRemovable = true;
    boot.loader.grub.fsIdentifier = "label";
    # Define on which hard drive you want to install Grub.
    boot.loader.grub.devices = [ "nodev" ]; # or "nodev" for efi only
    boot.loader.grub.useOSProber = true;
    boot.loader.grub.extraEntries = ''
      menuentry "Reboot" {
        reboot
      }
      menuentry "Poweroff" {
        halt
      }
    '';
    boot.loader.systemd-boot.enable = true;

    networking.hostName = "adrenaline";
    networking.wireless.enable = false;
    networking.interfaces.enp4s0.useDHCP = true;

    systemd.targets.sleep.enable = false;
    systemd.targets.suspend.enable = false;
    systemd.targets.hibernate.enable = false;
    systemd.targets.hybrid-sleep.enable = false;

    programs.steam.enable = true;
    powerManagement.enable = false;
    services.xserver.dpi = 180;
    services.xserver.layout = "gb";
    # Nvidia HW drivers.
    services.xserver.videoDrivers = [ "nvidia" ];
    hardware.opengl.enable = true;
    hardware.nvidia.powerManagement.enable = true;
    services.xserver.screenSection = ''
      Option         "metamodes" "nvidia-auto-select +0+0 {ForceFullCompositionPipeline=On}"
      Option         "AllowIndirectGLXProtocol" "off"
      Option         "TripleBuffer" "on"
    '';

    services.xserver.resolutions = [
      { x = "3840"; y = "2160"; }
    ];
    services.xserver.displayManager.setupCommands = ''
    ${pkgs.xlibs.xrandr}/bin/xrandr --output DP-0 --off --output DP-1 --off --output DP-2 --primary --mode 3840x2160 --pos 0x0 --rotate normal --output DP-3 --off --output HDMI-0 --mode 3840x2160 --pos 3840x0 --rotate normal --output DP-4 --off --output DP-5 --off 
    '';
    hardware.ledger.enable = true;
    environment = {
      # Desktop specific packages.
      systemPackages = with pkgs; [
        discord
        audacity
        gnucash
        airshipper
        vscodium-with-extensions

        # :thinking_face
        minecraft
        (wineWowPackages.full.override {
          wineRelease = "staging";
          mingwSupport = true;
        })
      ];
    };

    nixpkgs.overlays = [ (self: super: {
      godot-mono = with super;
      let
        arch = "64";
        version = "3.3.2";
        releaseName = "stable";
        subdir = "";
        pkg = stdenv.mkDerivation  {
          name = "godot-mono-unwrapped";
          buildInputs = [ unzip ];
          unpackPhase = "unzip $src";
          version = version;
          src = fetchurl {
            url = "https://downloads.tuxfamily.org/godotengine/${version}${subdir}/mono/Godot_v${version}-${releaseName}_mono_x11_${arch}.zip";
            sha256 = "sha256-imyJ/oYjZ++EeEdN99JrUuBapMr40PSuGWcuRTEc0Qw=";
          };
          installPhase = ''
            cp -r . $out
          '';
        };
      in buildFHSUserEnv {
        name = "godot-mono";
        targetPkgs = pkgs: (with pkgs;
          [ alsaLib
            dotnetCorePackages.sdk_5_0
            libGL
            libpulseaudio
            libudev
            xorg.libX11
            xorg.libXcursor
            xorg.libXext
            xorg.libXi
            xorg.libXinerama
            xorg.libXrandr
            xorg.libXrender
            zlib
          ]);
        runScript = "${pkg.outPath}/Godot_v${version}-${releaseName}_mono_x11_${arch}/Godot_v${version}-${releaseName}_mono_x11.${arch}";
      };
    }) ];
}
