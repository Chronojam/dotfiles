{ config, pkgs, ... }:

# List of common environment details
{
  environment = {
   variables = {
     GDK_SCALE = "2";
     GDK_DPI_SCALE = "0.75";
     _JAVA_OPTIONS = "-Dsun.java2d.uiScale=2";
     EDITOR = pkgs.lib.mkOverride 0 "vim";
   };
   systemPackages = with pkgs; [
      vim
      wget
      git
      firefox
      rxvt_unicode
      sxiv
      dmenu
      scrot
      pass
      xclip
      xorg.xmodmap
      feh
      dig
      haskellPackages.xmobar

      # Other important bits.
      libreoffice
      dunst
      spotify
    ];
  };
}
